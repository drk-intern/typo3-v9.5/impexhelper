<?php

namespace KayStrobach\Impexphelper\Hooks;

use KayStrobach\Impexphelper\Service\RenderExportInfoService;
use KayStrobach\Impexphelper\Slots\TcaManipulationSlot;
use TYPO3\CMS\Backend\Utility\BackendUtility;

class PageLayoutHeader
{
    /**
     * @return string
     */
    public function render()
    {
        $moduleData = BackendUtility::getModuleData(['language'], [], 'web_layout');
        $pageId = (int)$_GET['id'];

        $row = BackendUtility::getRecord('pages', (int)$pageId);

        $service = new RenderExportInfoService();
        return $service->render($row);
    }
}
