<?php
/**
 * Created by kay.
 */

namespace KayStrobach\Impexphelper\Slots;


class SqlManipulationSlot
{
    public function addHelperFieldsToAllTables(array $tableDefinition)
    {
        // @todo check why ` NOT NULL` is not allowed here
        foreach ($GLOBALS['TCA'] as $tableName => $tableConf) {
            $tableDefinition[] = 'CREATE TABLE ' . $tableName . ' (' . TcaManipulationSlot::FIELDNAME . ' varchar(255) DEFAULT \'-1\');';
        }

        return [$tableDefinition];
    }
}
