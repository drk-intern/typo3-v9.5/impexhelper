<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: 'contentmigration'
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * 'version' and 'dependencies' must not be touched!
 ***************************************************************/

$EM_CONF['impexphelper'] = array(
    'title' => 'imexphelper',
    'description' => 'Allows to mark pages for special installations',
    'category' => 'templates',
    'author' => '4viewture UG (haftungsbeschränkt) - Kay Strobach',
    'author_email' => 'ks@4viewture.eu',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '9.12.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '9.5.0-9.5.99',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    'autoload' => array(
        'psr-4' => array(
            'KayStrobach\\Impexphelper\\' => 'Classes/'
        )
    )
);
