<?php

namespace KayStrobach\Impexphelper\Hooks\Form;


use KayStrobach\Impexphelper\Slots\TcaManipulationSlot;
use TYPO3\CMS\Backend\Form\FormDataProviderInterface;
use TYPO3\CMS\Backend\Utility\BackendUtility;

class AllRecordsRowInitializeNew implements FormDataProviderInterface
{

    public function addData(array $result)
    {
        if ($result['command'] !== 'new') {
            return $result;
        }
        if (isset($result['databaseRow'][TcaManipulationSlot::FIELDNAME])) {
            $result['databaseRow'][TcaManipulationSlot::FIELDNAME] = explode(',', $result['parentPageRow'][TcaManipulationSlot::FIELDNAME]);
        }
        return $result;
    }
}
