<?php

return [
    'impexphelper:tree' => [
        'class' => \KayStrobach\Impexphelper\Command\TreeCommand::class,
        'schedulable' => false,
    ],
];
