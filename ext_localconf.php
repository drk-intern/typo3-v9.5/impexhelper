<?php

/**
 * Draw export info for content
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['impexphelper']
    = \KayStrobach\Impexphelper\Hooks\ContentPreview::class;

/**
 * Draw export info for pages
 */
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/db_layout.php']['drawHeaderHook'][]
    = \KayStrobach\Impexphelper\Hooks\PageLayoutHeader::class . '->render';

/**
 * Initialize Data for new records
 */
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['formDataGroup']['tcaDatabaseRecord'][\KayStrobach\Impexphelper\Hooks\Form\AllRecordsRowInitializeNew::class] = [
    'depends' => [
        \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowInitializeNew::class,
    ]
];

call_user_func(
    static function () {
        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);

        // connect to tca and add all the needed config for the selector menu
        $signalSlotDispatcher->connect(
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::class,  // Signal class name
            'tcaIsBeingBuilt',                                          // Signal name
            \KayStrobach\Impexphelper\Slots\TcaManipulationSlot::class, // Slot class name
            'addHelperFieldsToAllTables'                                // Slot name
        );

        // connect to sql reader and inject all the needed fields to all the tables
        $signalSlotDispatcher->connect(
            'TYPO3\\CMS\\Install\\Service\\SqlExpectedSchemaService',   // Signal class name
            'tablesDefinitionIsBeingBuilt',                             // Signal name
            \KayStrobach\Impexphelper\Slots\SqlManipulationSlot::class, // Slot class name
            'addHelperFieldsToAllTables'                                // Slot name
        );
    }
);
